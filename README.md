# README #

### What is this repository for? ###

* Frontend para apresentacao do cálculo de funções lineares de maximização e minimização utilizando algoritmo simplex, que são realizados por uma API
* v1.2

### How do I get set up? ###

* Clonar o repositorio
* Executar API
* Definir valor da variavel base_url no inicio do arquivo index.js com a url em que a api esta sendo servida
* Abrir o arquivo index.html em um browser (navegador)