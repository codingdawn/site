$(document).ready(function() {


    var base_url = "http://localhost:5000"


    var tabelas_response;
    console.log( 'ready!' );
    var tabelas_obj = 'ola';
    $('#gerar_modelo').on('click', function() {
        console.log($('#qtd_it_fake').val());
        $('#qtd_it').val($('#qtd_it_fake').val());
        console.log('Clicou btn send');

        // Capturar dados e gerar FUNCAO Z
        $('#container_funcao_z').html('');

        for (var i = 0; i < $('#qtd_var').val(); i++) {
            var x_input = $('#x_input').clone();
            var x_prepend = $('#x_prepend').clone();

            x_input.prop('placeholder', 'Valor de x'+(i+1))
                   .prop('name', 'funcao['+(i+1)+']')
                   .removeAttr('id')
                   .addClass('funcao_z')
                   .attr('style', '');
            plus = (i == $('#qtd_var').val()-1) ? '' : '+';
            x_prepend.find('.input-group-text').html('X'+(i+1)+plus);
            x_prepend.attr('style', '');

            $('#container_funcao_z').append(x_input);
            $('#container_funcao_z').append(x_prepend);
        }

        // Capturar dados e gerar Regras
        $('#container_regras').html('');
        for (var j = 0; j < $('#qtd_reg').val(); j++) {
            //console.log($('#qtd_reg').val(), $('#qtd_var').val());
            var regra = $('#exemplo_regra').clone();

            regra.find('label').html('Regra '+(j+1));
            regra.removeAttr('id')
                 .attr('style', '');

            for (var i = 0; i < $('#qtd_var').val(); i++) {
                var x_input = $('#x_input').clone();
                var x_prepend = $('#x_prepend').clone();
               // console.log("AAAAAAAAAAA");
               // console.log('Regra X '+i+'   '+$('#qtd_var').val()-1);
                x_input.prop('placeholder', 'Valor de x'+(i+1))
                       .prop('name', 'funcao['+(i+1)+']')
                       .addClass('regra'+j)
                       .removeAttr('id')
                       .attr('style', '');
                plus = (i == ($('#qtd_var').val())-1) ? '' : '+';
                x_prepend.find('.input-group-text').html('X'+(i+1)+plus);
                x_prepend.attr('style', '');

                regra.find('#container_regra').append(x_input);
                regra.find('#container_regra').append(x_prepend);
            }

            var x_input = $('#x_input').clone();
            var x_prepend = $('#x_prepend').clone();

            x_prepend.find('.input-group-text').html('&le;');
            x_prepend.removeAttr('id').attr('style', '');
            x_input.prop('placeholder', 'Valor da resposta '+(j+1))
                   .addClass('regra'+j)
                   .removeAttr('id').attr('style', '');

            regra.find('#container_regra').append(x_prepend);
            regra.find('#container_regra').append(x_input);

            $('#container_regras').append(regra);

        }

        $('#div_config').attr('style', 'display: none;');
        $('#div_modelo').attr('style', '');
        $('.div_qtd_it').attr('style', 'display: none;');
        $('.inf_pos').attr('style', 'display: none;');

    });

    $('.voltar_config').on('click', function() {
        $('#div_config').attr('style', '');
        $('#div_modelo').attr('style', 'display: none;');
        $('#div_result').attr('style', 'display: none;');
        $('.inf_pos').attr('style', 'display: none;');
    });

    $('.voltar_modelo').on('click', function() {
        $('#div_modelo').attr('style', '');
        $('#div_config').attr('style', 'display: none;');
        $('#div_result').attr('style', 'display: none;');
        $('.inf_pos').attr('style', 'display: none;');
        $('.div_qtd_it').attr('style', '');
    });

    $('#gerar_result').on('click', function() {
        $('#container_tabelas').html('');


        //var url = "http://localhost:5000/simplex/"+$('input[name="exampleRadios"]:checked').val();
        var url ="https://murmuring-harbor-88504.herokuapp.com/simplex/"+$('input[name="exampleRadios"]:checked').val();

        console.log(url);
        var resp = [], regras = [];

        for (var i = 0; i < $('#qtd_reg').val(); i++)
            regras.push($('.regra'+i).map(function(){return parseInt($(this).val(), 10);}).get());

        //* Nao apagar
        var data = {
            "qtd_var" : $('#qtd_var').val(),
            "qtd_regras" : $('#qtd_reg').val(),
            "iteracoes": parseInt($('#qtd_it').val()),
            "funcao" : $('.funcao_z').map(function(){return parseInt($(this).val(), 10);}).get(),
            "regras": regras
        };
        //*/
        /*
        var data = {
            "qtd_var": 2,
            "qtd_regras": 2,
            "iteracoes": parseInt($('#qtd_it').val()),
            "funcao": [11,12],
            "regras": [[1,4,10000],[5,2,30000]]
        };
        //*/
        console.log(data);

        $.ajax(
            {
                url: url,
                type: "POST",
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify(data),
                dataType: "json"

            }
        ).done(function (json) {
            console.log('Passou Post');
            console.log(json);
            if (json["infinito"]==true){
                $('.inf_pos').attr('style', '');
            }
            // resp = json.data;
            tabelas_response = json;
            $.each(json.data, function(key, val){resp[key] = val})

            $('#div_modelo').attr('style', 'display: none;');
            $('#div_result').attr('style', '');

            console.log('resp');
            console.log(resp);
            tabelas_obj = resp;

            $.each(resp, function( index, tb ) {
                tabla = $('#final_table').clone();
                tabla.html('').attr('id', 'tb'+(index + 1));

                // Montando o thead da tabla
                tabla.append('<thead class="thead-dark"><tr><th colspan=' + ($('#qtd_var').val() + $('#qtd_reg').val() + 2) + ' scope="col">Iteração ' + (index + 1) + '</th></tr></thead>');

                tabla.append('<thead class="thead-dark" id="thead"><tr><th scope="col">Base</th></tr></thead><tbody></tbody>');
                for (var i = 0; i < $('#qtd_var').val(); i++)
                    tabla.find('#thead').find('tr').append('<th scope="col">X'+(i+1)+'</th>');
                for (var i = 0; i < $('#qtd_reg').val(); i++)
                    tabla.find('#thead').find('tr').append('<th scope="col">F'+(i+1)+'</th>');
                tabla.find('#thead').find('tr').append('<th scope="col">b</th>');

                // Montando o corpo da tabla
                for (var i = 0; i < tb['labels'].length; i++) {
                    var atual = tb['labels'][i];
                    tabla.find('tbody').append('<tr class="'+atual+'"></tr>');
                    tabla.find('.'+atual).append('<th scope="col">'+atual+'</th>');

                    console.log(tb[atual]);
                    for (var j = 0; j < tb[atual].length; j++) {
                        tabla.find('.'+atual).append('<th scope="col">'+parseFloat(tb[atual][j].toFixed(2))+'</th>');
                    }
                }
                //tabla.append('<div class="row"><p></p></div>');
                // $('#container_tabelas').append('<div class="row">'+tabla.html()+'</div>');
                $('#container_tabelas').append(tabla);


            });

            console.log("json.length",json.data.length);
            console.log("json", json.data[json.data.length-1]);

            $.ajax(
                {

                    url: "https://murmuring-harbor-88504.herokuapp.com/simplex/precosoma",

                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    data: JSON.stringify(json.data[json.data.length - 1]),
                    dataType: "json"

                }
            ).done(function (json) {

                console.log("SASASASAS",json);
                tabla_sombra = $('#sombra_table').clone();
                tabla_sombra.html('').attr('id', 'tb_sombra');


                tabla_sombra.append('<thead class="thead-dark"><tr><th colspan="5" scope="col">Preço Sombra da Ultima Solução</th></tr></thead>');
                tabla_sombra.append('<thead class="thead-dark" id="thead"><tr><th scope="col">Base</th><th scope="col">V.F</th><th scope="col">P.S</th><th scope="col">+</th><th scope="col">-</th></tr></thead><tbody></tbody>');

                json.labels.forEach(function (val) {


                    tabla_sombra.find('tbody').append('<tr class="' + val + '"></tr>');
                    tabla_sombra.find('.' + val).append('<th scope="col">' + val + '</th>');

                    for (var j = 0; j < json[val].length; j++) {
                        var aux;
                        if (typeof json[val][j] == "string"){
                            aux = "-";
                        }else{
                            aux = parseFloat(json[val][j]).toFixed(2);
                        }
                        tabla_sombra.find('.' + val).append('<th scope="col">' + aux + '</th>');
                    }

                });

                /*for (var i = 0; i < tb['labels'].length; i++) {
                    var atual = tb['labels'][i];
                    tabla.find('tbody').append('<tr class="' + atual + '"></tr>');
                    tabla.find('.' + atual).append('<th scope="col">' + atual + '</th>');

                    console.log(tb[atual]);
                    for (var j = 0; j < tb[atual].length; j++) {
                        tabla.find('.' + atual).append('<th scope="col">' + parseFloat(tb[atual][j].toFixed(2)) + '</th>');
                    }
                }*/

                $('#container_tabelas').append(tabla_sombra);


            }).fail(function (json) {
                console.log('Error Post');
                console.log(json);

            });

        }).fail(function (json) {
            console.log('Error Post');
            console.log(json);
            resp = json;

        });
    });



});
